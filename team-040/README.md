# Research project
This project was made for the EDOM curricular unit and has as its theme the use of the Modisco framework in reverse engineering.
## Reverse engineering
At first lets look what Reverse engineering is. Reverse engineering is the process of extracting the knowledge or design blue-
prints from anything man-made. The concept has been around since long before computers or modern technology. 
It is very similar to scientific research, in which a researcher is attempting to work out the “blueprint” of the atom or the human
mind. The difference between reverse engineering and conventional scientific research is that with reverse engineering the artifact being investigated is man-
made, unlike scientific research where it is a natural phenomenon. Reverse engineering is usually conducted to obtain missing knowledge,
ideas, and design philosophy when such information is unavailable. In some cases, the information is owned by someone who isn’t willing to share them.
In other cases, the information has been lost or destroyed.[3]

Reverse engineering has foud place in software developements in many forms, from extracting code from existing binary, to generating models of existing project.
Reconstruction plays an important role in increasing the quality and productivity of software development. It involves
activities for abstracting information that ranges from source-level information to higher-level views of that
information. Reverse engineering is the key idea for reconstruction of any existing system.[1]

Reverse engineering is the process of comprehending software and producing a model of it at a high level of abstraction, suitable for
documentation, maintenance or reengineering. How can reverse engineering be used to help
solve the problem of foreign code? Reverse engineering technology has, in fact, proven 
useful on many projects in helping a maintenance team obtain a better understanding of
the structure and functioning of a software system.[2]
## Modisco
#### Introduction to the framework

Inn the context of computing a legacy system refers to outdated computer systems, programming languages or application software that are used instead of available upgraded versions. This systems embrace a large number of technologies, making the development of tools to cope with legacy systems evolution a tedious and time consuming task.

Model-Driven Engineering (MDE) refers to the systematic use of models as primary engineering artifacts throughout the engineering lifecycle. Model-Driven Reverse Engineering (MDRE) is the application of MDE principles and techniques to Reverse Engineering in order to generate relevant model-based views on legacy systems, thus facilitating their understanding and manipulation. 

In this context, MDRE is practically used in order to:

 1. discover initial models from the legacy artifacts composing a given
    system;  
 2. understand (process) these models to generate relevant
        views (i.e., derived models) on this system.

MoDisco is thus an official Eclipse-Model Driven Technology project dedicated to Model Driven Reverse Engineering (MDRE) from IT legacy systems. 

#### Framework features

This project provides a set of extensible and reusable components, which form a generic framework allowing the elaboration of concrete solutions to support the specificities of different reverse engineering scenarios.

The Modisco can be used as part of modernization processes (technology migration, software refactoring, architecture restructuring, etc), but also for retro-documentation, quality assurance or basically any kind of use case requiring a better understanding of existing systems.In addition to this, it already provides an advanced support for the Java, JEE and XML technologies, including complete metamodels, corresponding discoverers and generators, customizations and query libraries for example.

MoDisco also offers a set of model-based components allowing elaborating on reverse engineering solutions. The concerned legacy systems (to be “reverse engineered”) can be of varied and various natures. In addition to its technology-independent components, MoDisco natively provides an extensive support for Java legacy systems.

In the case of Java, the metamodel implemented in MoDisco and directly usable covers the full Java language and constructs: it specifies everything a Java program can contain from the global structure (i.e. packages, classes, dependencies) to the internal one (i.e. detailed instructions and statements inside the methods). Thus, it is possible to completely represent an existing program as a (Java) model.

As previously stated, one of the main characteristics of legacy systems is their important size and complexity. To directly address this, MoDisco provides as part of its framework a customizable generic Model Browser. It can be used to more easily browse all EMF models (independently from their metamodels) and provides advanced features such as full exploring, infinite navigation, filtering, sorting, searching, etc.

MoDisco also has an integrated mechanism to define and execute queries over the handled models. These queries can be implemented in Java, ATL, or OCL. MoDisco also comes with “generators” which allows producing the Java source code from the handled Java models. 

#### Framework layers
In general this framework is composed of 3 layers:

The “Infrastructure” layer is the base of the framework. It provides the set of generic components (in the sense of legacy technology-independence) that can be reused in various and varied reverse engineering scenarios.

The “Technologies” layer is made to be used on top of components from the “Infrastructure”. It implements the legacy technology-specific support via the corresponding reference metamodels, discoverers, generators, transformations, etc.

Finally, the “Use Cases” layer is just here to present a couple of sample scenarios showing how the different components, from the two previously described layers, can be integrated together via the framework in order to achieve a particular reverse engineering goal. 

These three layers, and the natural complementarity between their contained generic and technology-specific components, give the MoDisco framework all the extensibility and reusability capabilities required to be able to elaborate on concrete reverse engineering solutions.

All the presented features of this framework are very useful when dealing with reverse engineering from legacy systems.

## Case Study

In this case study we will give an example of how it would be possible to obtain a UML model through a Java model. For this use case the sources of the Pet Store application were used as the starting point. These sources are available at the following link: https://www.oracle.com/technetwork/java/petstore1-3-1-02-139690.html

### Steps
#### Get the java model
  
To obtain the Java model of the sources mentioned above, a MoDisco Discovery Run Configuration was created. In this configuration we use discoverer *org.eclipse.modisco.java.discoverer.project*.
![alt text](print1.png)

#### Get the KDM model

To transform the previously obtained Java model in a KDM model, we had to select the model and then select the Discovery option followed by the Discoverers and Discover KDM model from Java model option
![alt text](print2.png)

#### Get the UML model

In this step we transformed  the KDM model into a UML model. In this step we had to select the KDM model previously obtained and then select the Discovery option followed by the Discoverers and Discoverer UML model from KDM model option.
![alt text](print3.png)

#### Visualize the UML model

In order to visualize the model the Modisco model browser was used. The MoDisco model browser displays the model as a tree. It proposes a view of all instances sorted by metaclass, and offers the possibility to view all links in the model (if empty or not, with cardinality, etc.). 
![alt text](print4.png)

## Approach lectured vs reverse engineering
The major goal of EDOM course is to provide advanced skills regarding domain engineering and some of the most recent scientific innovations and practices originating from specific approaches that are related to it: model-driven engineering, domain-specific languages and product line engineering.This approach promotes the use of processes, tools and methodologies to achieve large scale reuse of product lines.

Most companies build similar systems, making only a few adjustments to meet the customer requirements. With this approach, those companies can reuse the common parts instead of rebuilding every part of the system for each solution. It has a negative impact in the cost and time of the production in early stages, however it can greatly increasing the profit made in the later ones.

In legacy systems, this type of approach can be hard to implement since the system is in development for a long time. In this case, reverse engineering can help on the redocumentation and design recovery of the project. From there it is easier to understand the code and model.

Although they are very distinct approaches, they can actually complete each other. In products that are already in development, or finished products, it is hard to achieve large scale reuse since it wasn't thought of in the early stages. That way, it is possible to reverse engineer the system, generating new and updated documentation that will help with the implementation of a model-driven and domain-specific system.

## Literature Review
Reverse engineering is brought subject, looking at subject from software developement view we can find information how to extract code from existing binary.
This conccern is described in Secreets of Reverse Engineering by E. Wiliams.
During EDOM subjects we focused more about translation from model to code and the other way.
Examples and description of Model-Driven Reverse Engineering can be found in paper writen by  Spencer Rugaber and Kurt Stirewalt.
In paper by Vinita, Amita Jain, Devendra K. Taylar, there is description of reverse engineering from object-oriented code int UML. 
Paper contain algorithm and its mostly aimed to C++, but general   aproaches described in paper stays the same for object oriented code not neccecery writen in C++.

#### References
[1]On reverse engineering an object-oriented code into UML class diagrams incorporating extensible mechanisms(May 2007)
Vinita, Amita Jain, Devendra K. Tayal

[2]Model-Driven Reverse Engineering
Spencer Rugaber (spencer@cc.gatech.edu)
Georgia Institute of Technology
Kurt Stirewalt (stire@cps.msu.edu)
Michigan State University

[3]Reversing - Secrets of Reverse Engineering - E. Eilam (Wiley, 2005)

https://hal.inria.fr/hal-00972632/document

https://jaxenter.com/modisco-in-a-nutshell-103816.html

https://www.eclipse.org/MoDisco/

https://wiki.eclipse.org/MoDisco/SimpleTransformationChain